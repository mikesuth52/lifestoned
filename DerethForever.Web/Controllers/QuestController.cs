﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Quests;
using Lifestoned.DataModel.Shared;
using Lifestoned.Providers;


namespace DerethForever.Web.Controllers
{
    public class QuestController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IQuestTableProvider Provider => ContentProviderHost.GetProvider<IQuestTableProvider>(QuestChange.TypeName);

        private IQuestTableSandboxProvider SandboxProvider => SandboxContentProviderHost.GetProvider<IQuestTableSandboxProvider>(QuestChange.TypeName);

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Get(uint id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Guid userId = new Guid(GetUserGuid());
                ChangeEntry<QuestTableEntry> change = SandboxProvider.GetChange(userId, id);
                if (change != null)
                    return JsonGet(change.Entry, true);
            }

            QuestTableEntry model = Provider.Get(id);
            return JsonGet(model, true);
        }

        [HttpGet]
        public ActionResult Search(string name)
        {
            IQueryable<QuestTableEntry> model = null;
            if (User.Identity.IsAuthenticated)
            {
                model = SandboxProvider.Search(new Guid(GetUserGuid()), (s) => s.Key == name || s.Value.Description == name);
            }
            else
            {
                model = Provider.Search((s) => s.Key == name || s.Value.Description == name);
            }

            return JsonGet(model, true);
        }

        [HttpPut]
        [Authorize]
        public ActionResult Put([ModelBinder(typeof(JsonNetModelBinder))]QuestTableEntry model)
        {
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            if (model.Id == null || model.Id == 0)
                model.Id = (uint)model.Key.GetHashCode();

            Guid userId = new Guid(GetUserGuid());

            try
            {
                SandboxProvider.UpdateChange(userId, model);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error($"Error Saving Quest {model.Key} - {model.Value.Description}", ex);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError, "Save Failed");
        }

        [HttpGet]
        public ActionResult Edit(uint? id)
        {
            return View(id);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(uint id)
        {
            return View(Provider.Get(id));
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(QuestTableEntry model)
        {
            Provider.Delete(model.Id.Value);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult UploadItem()
        {
            string fileNameCopy = "n/a";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string token = GetUserToken();
                        string userName = GetUserName();

                        string serialized = Encoding.UTF8.GetString(data);
                        QuestTableEntry[] table = JsonConvert.DeserializeObject<QuestTableEntry[]>(serialized);

                        foreach (QuestTableEntry entry in table)
                        {
                            entry.LastModified = DateTime.Now;
                            entry.ModifiedBy = userName;

                            if (string.IsNullOrEmpty(entry.Key))
                                continue;   // bad

                            if (entry.Id == null || entry.Id == 0)
                                entry.Id = (uint)entry.Key.GetHashCode();

                            // save it
                            Provider.Save(entry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded file {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        ////[HttpGet]
        ////public ActionResult DownloadOriginal(uint id)
        ////{
        ////    try
        ////    {
        ////        QuestTableEntry model = Provider.Get(id);
        ////        return DownloadJson(model, id, model.Key);
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        log.Error($"Error exporting quest {id}", ex);
        ////        return RedirectToAction("Index");
        ////    }
        ////}

        ////[HttpGet]
        ////[Authorize]
        ////public ActionResult DownloadSandbox(uint id, string userGuid)
        ////{
        ////    Guid userId = Guid.Empty;

        ////    if (!string.IsNullOrWhiteSpace(userGuid) && (userGuid == GetUserGuid() || User.IsInRole("Developer")))
        ////    {
        ////        userId = new Guid(userGuid);
        ////    }
        ////    else
        ////    {
        ////        userId = new Guid(GetUserGuid());
        ////    }

        ////    try
        ////    {
        ////        ChangeEntry<QuestTableEntry> change = SandboxProvider.GetChange(userId, id);
        ////        if (change == null)
        ////        {
        ////            return HttpNotFound();
        ////        }

        ////        return DownloadJson(change.Entry, change.EntryId, change.Entry.Key);
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        log.Error($"Error exporting quest {id}", ex);
        ////        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
        ////    }
        ////}

    }
}