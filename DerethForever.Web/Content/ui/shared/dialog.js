﻿/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-dialog', {
    props: ['title'],
    methods: {
        show() {
            $(this.$refs.container).modal('show');
        },

        hide() {
            $(this.$refs.container).modal('hide');
        },

        saved() {
            this.$emit('saved');
            this.hide();
        }

    },
    template: `
    <div ref="container" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title col-md-6">{{ title }}</h4>
                    <slot name="headerCommands"></slot>
                </div>
                <div class="modal-body">
                <slot></slot>
                </div>
                <div class="modal-footer">
                    <button @click="saved" type="button" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    `
});