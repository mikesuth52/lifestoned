﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-quest-table-search', {
    data() {
        return {
            name: null
        };
    },
    computed: {
    },
    watch: {
    },
    methods: {
        search() {
            this.$questTable.search(this.name);
        }
    },
    template: `
    <div class="well">
        <div class="row row-spacer">
            <div class="col-md-9">Name</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-9"><input v-model="name" type="text" class="form-control" /></div>
            <div class="col-md-1 col-md-offset-2"><button @click="search" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></div>
            <!-- div class="col-md-2"></div -->
        </div>
    </div>
    `
});

Vue.component('lsd-quest-table', {
    data() {
        return {
            current: this.$questTable.newQuest()
        };
    },
    computed: {
        results() { return this.$questTable.table; }
    },
    methods: {
        edit(entry) {
            this.current = entry;
            this.$refs.entry.show();
        }
    },
    template: `
    <div>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Last Modified</th>
                <th style="width: 16rem; text-wrap:none;">Actions</th>
            </tr>
        </thead>
        <tbody>
        <tr v-for="entry in results">
            <td>{{ entry.key }}</td>
            <td>{{ entry.value.fullname }}</td>
            <td>{{ entry.lastModified | toDate }}</td>
            <td>
                <div class="btn-group" role="group">
                    <button @click="edit(entry)" type="button" class="btn btn-sm btn-default">
                        <span class="glyphicon glyphicon-edit" title="Edit"></span>
                    </button>

                    <div class="btn-group dropdown" role="group">
                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"><span class="glyphicon glyphicon-download" title="Download"></span>&nbsp;<span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a :href="'/SpellTable/DownloadOriginal/' + entry.id">Original</a></li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <lsd-quest-table-entry ref="entry" v-model="current"></lsd-spell-quest-entry>
    </div>
    `
});

Vue.component('lsd-quest-table-entry', {
    props: ['entry'],
    model: { prop: 'entry', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        quest() {
            return this.entry.value;
        },
        title() {
            return `Edit ${this.entry.key}`;
        }
    },
    methods: {
        show() {
            this.$refs.modal.show();
        },
        save() {
            this.$questTable.save(this.entry);
        }
    },
    template: `
    <lsd-dialog ref="modal" :title="title" @saved="save">
        <template v-slot:headerCommands>
        <div class="col-md-offset-2 col-md-4">

            <ul class="nav nav-pills nav-pills-sm" role="group">
                <li class="active"><a data-toggle="tab" href="#general" class="btn btn-primary" title="General Properties"><i class="glyphicon glyphicon-list"></i></a></li>
                <li><a data-toggle="tab" href="#history" class="btn btn-primary" title="Change Log / History"><i class="glyphicon glyphicon-calendar"></i></a></li>
            </ul>
        </div>
        </template>

        <div class="tab-content">

            <div id="general" class="tab-pane fade in active">
                <lsd-panel title="General" stickHeader :showHeader="false">
                    <div class="row">
                        <div class="col-md-4">Max Solves</div>
                        <div class="col-md-4">Min Time</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><input v-model="entry.value.maxsolves" type="number" class="form-control" /></div>
                        <div class="col-md-4"><input v-model="entry.value.mindelta" type="number" class="form-control" /></div>
                    </div>

                    <div class="row"><div class="col-md-8">Description</div></div>
                    <div class="row">
                        <div class="col-md-8"><input v-model="entry.value.fullname" type="text" class="form-control" /></div>
                    </div>

                </lsd-panel>
            </div>

            <div id="history" class="tab-pane fade">
                <lsd-changelog v-model="entry" :showHeader="false"></lsd-changelog>
            </div>

        </div>

    </lsd-dialog>
    `
});
