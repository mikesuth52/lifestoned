﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.DataModel.Gdle.Quests
{
    public class QuestTableEntry : IMetadata
    {
        [JsonProperty("id")]
        public uint? Id { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("value")]
        public QuestInfo Value { get; set; }

        #region Metadata

        [JsonProperty("lastModified")]
        [Display(Name = "Last Modified Date")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("modifiedBy")]
        [Display(Name = "Last Modified By")]
        public string ModifiedBy { get; set; }

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("userChangeSummary")]
        public string UserChangeSummary { get; set; }

        [JsonProperty("isDone")]
        [Display(Name = "Is Done")]
        public bool IsDone { get; set; }

        #endregion
    }

    public class QuestInfo
    {
		[JsonProperty("fullname")]
		public string Description { get; set; }

		[JsonProperty("maxsolves")]
		public int MaxSolves { get; set; }

		[JsonProperty("mindelta")]
		public int MinDelta { get; set; }
    }

    public class QuestChange : ChangeEntry<QuestTableEntry>
    {
        public const string TypeName = "quest";

        [JsonIgnore]
        public override uint EntryId
        {
            get => Entry.Id.Value;
            set => Entry.Id = value;
        }

        [JsonIgnore]
        public override string EntryType => QuestChange.TypeName;

        [JsonProperty("quest")]
        public override QuestTableEntry Entry { get; set; }
    }

}
