﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Content
{
    //    CREATE TABLE IF NOT EXISTS Patch
    public class Patch
    {
        //    id INT PRIMARY KEY
        public int Id { get; set; }

        //    lastModified DATETIME
        public DateTime LastModified { get; set; }

        //    userModified VARCHAR(100)
        public string UserModified { get; set; }

        //    name VARCHAR(250)
        public string Name { get; set; }

        //    releaseDate DATETIME
        public DateTime ReleaseDate { get; set; }

        //    complete INT DEFAULT 0 NOT NULL
        public bool Complete { get; set; }

        //    completeDate DATETIME
        public DateTime CompleteDate { get; set; }
    }
}
